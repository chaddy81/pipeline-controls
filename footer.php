<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
	</section>

        <!-- Footer -->
        <footer>
            <div class="clearfix">
                <!-- Colums -->
                <div class="columns">
					<?php wp_nav_menu( array('menu' => 'Primary' )); ?>
                    <h5>Offices</h5>
                    <div class="column vcard">
                        <div class="adr">
                            <p><strong><span class="region" title="Atlanta, Georgia">Atlanta, Georgia</span></strong></p>
                            <p><span class="tel">770 619 5666</span></p>
                            <p>
                                <span class="street-address">235 Hembree Park Dr<br />Ste 100</span><br />
                                <span class="locality">Roswell</span>, <abbr class="region" title="Georgia">GA</abbr> <span class="postal-code">30076</span>
                            </p>
                        </div>
                    </div>
                    <div class="column vcard">
                        <div class="adr">
                            <p><strong><span class="region" title="Madisonville, Louisiana">Madisonville, Louisiana</span></strong></p>
                            <p><span class="tel">985 845 7384</span></p>
                            <p>
                                <span class="street-address">198 Daine Lane</span><br />
                                <span class="locality">Madisonville</span>, <abbr class="region" title="Louisiana">LA</abbr> <span class="postal-code">70447</span>
                            </p>
                        </div>
                    </div>
                    <div class="column vcard">
                        <div class="adr">
                            <p><strong><span class="region" title="Durango, Colorado">Durango, Colorado</span></strong></p>
                            <p><span class="tel">907 845 7384</span></p>
                            <p>
                                <span class="street-address">649 Tech Center Drive<br />Suite B</span><br />
                                <span class="locality">Durango</span>, <abbr class="region" title="Colorado">CO</abbr> <span class="postal-code">81301</span>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- Logo -->
                <div class="logo">
					<a href="index.html"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/footer-logo.gif" alt="Pipeline Controls &amp; Services" /></a>
                </div>
            </div>
        </footer>
    </div>
</body>
</html>
