<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?><!doctype html>
<html>
<head>
    <meta charset=utf-8>
    <title>Pipeline Controls &amp; Services</title>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/js/cycle.js"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/js/main.js"></script>
    <!--[if lt IE 9]>
        <script>
           document.createElement('header');
           document.createElement('nav');
           document.createElement('section');
           document.createElement('article');
           document.createElement('aside');
           document.createElement('footer');
        </script>
    <![endif]-->
</head>
<body>
    <div id="page">
        <!-- Header -->
        <header>
            <!-- Head -->
            <div id="head" class="clearfix">
                <!-- Phone -->
                <div id="phone">
                    <p>National Service. Call Now!</p>
                    <span class="phone">770 619 5666</span>
                </div>
                <!-- Logo -->
				<h1 class="logo"><a href="<?php echo home_url(); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.gif" alt="Pipeline Controls &amp; Services" /></a></h1>
            </div>
            <!-- Menu -->
			<div id="menu">
				<?php wp_nav_menu( array('menu' => 'Primary' )); ?>
            </div>
        </header>

        <!-- Content -->
        <section id="content">
