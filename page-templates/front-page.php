<?php
/**
 * Template Name: Standard Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package Pipeline Controls
 * @subpackage Pipeline_Controlse
 * @since 2013
 */

get_header(); ?>

	<!-- Slider -->
	<div id="slider" class="home loading">
		<div class="slide">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pictures/slide-home.jpg" alt="" />
			<div class="title"></div>
		</div>
		<div class="slide">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pictures/slide-home-2.jpg" alt="" />
			<div class="title"></div>
		</div>
		<div class="slide">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pictures/slide-home-3.jpg" alt="" />
			<div class="title"></div>
		</div>
	</div>
	<!-- Main Content -->
	<div id="main" class="clearfix">
		<!-- Thumbs -->
		<?php get_sidebar( 'front' ); ?>
		<!-- Text -->
		<div id="text">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
			<?php endwhile; ?>
		</div>
	</div>

<?php get_footer(); ?>
