<?php
/**
 * Template Name: Other Services Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package Pipeline Controls
 * @subpackage Pipeline_Controlse
 * @since 2013
 */

if (is_page('other-services')) {
	$title = "Application Services";
} else {
	$title = single_post_title('', false);
}
get_header(); ?>

	<!-- Content -->
	<section id="content">
		<!-- Slider -->
		<div id="slider" class="loading">
			<div class="slide">
				<div class="title">
					<h4>Other Services</h4>
					<h3><?php echo $title; ?></h3>
				</div>
			</div>
		</div>
		<!-- Main Content -->
		<div id="main" class="sub clearfix">
			<!-- Sidebar -->
			<div id="sidebar">
				<?php wp_nav_menu( array('menu' => 'Sidebar' )); ?>
			</div>
			<!-- Text -->
			<div id="text">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', 'page' ); ?>
				<?php endwhile; ?>
			</div>
		</div>
	</section>

<?php get_footer(); ?>
