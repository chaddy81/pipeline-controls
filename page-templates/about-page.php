<?php
/**
 * Template Name: About Us Page
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package Pipeline Controls
 * @subpackage Pipeline_Controlse
 * @since 2013
 */

if (is_page('other-services')) {
	$title = "Application Services";
} else {
	$title = single_post_title('', false);
}
get_header(); ?>

	<!-- Slider -->
	<div id="slider" class="loading">
		<div class="slide">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pictures/controls-slide.jpg" alt="" />
			<div class="title">
				<h3>About Us</h3>
			</div>
		</div>
	</div>
	<!-- Main Content -->
	<div id="main" class="about clearfix">
		<!-- Thumbs -->
		<?php get_sidebar( 'about' ); ?>
		<!-- Text -->
		<div id="text">
			<div class="excerpt">
				<p>The history of our company is relatively short, but our experience is long. 
					Incorporated in April 2005, Pipeline Controls &amp; Servicesi core team of consultants have been working 
					together since 1998. Jack Brooks, Josh Harrell, and Russell Pate were building scheduling 
					systems in the pipeline industry when they saw the need for a nimble company that could 
					provide world-class software and project solutions to major pipeline operators.
				</p>
			</div>
			<h5>Employment Opportunities</h5>
			<p>We are always looking for the talented, hard-working individuals for our team. We are looking for the following roles:</p>
			<?php query_posts( 'post_type=pipeline_jobs'); ?>

			<ul>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>    
				<li><a href="<?php echo get_post_meta($post->ID, 'jobs_url', true); ?>"><?php the_title(); ?></a></li>
            <?php endwhile; endif; ?>
			</ul>
			<h5>Associations</h5>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec scelerisque a ante eget aliquet. Donec ac nisi in purus sagittis tincidunt nec vel ante. Integer imperdiet faucibus metus eget porta. Praesent sagittis, nunc et molestie dictum, odio felis volutpat eros, vitae bibendum augue elit id diam.</p>
			<div class="partners">
				<a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pictures/assoc-1.gif" alt="" /></a>
				<a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pictures/assoc-2.gif" alt="" /></a>
				<a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pictures/assoc-3.gif" alt="" /></a>
				<a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pictures/assoc-4.gif" alt="" /></a>
				<a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pictures/assoc-5.gif" alt="" /></a>
			</div>
			<h5>Sponsorship</h5>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec scelerisque a ante eget aliquet. Donec ac nisi in purus sagittis tincidunt nec vel ante. Integer imperdiet faucibus metus eget porta. Praesent sagittis, nunc et molestie dictum, odio felis volutpat eros, vitae bibendum augue elit id diam.</p>
			<div class="partners">
				<a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pictures/sponsor-1.gif" alt="" /></a>
				<a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pictures/sponsor-2.gif" alt="" /></a>
				<a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pictures/sponsor-3.gif" alt="" /></a>
				<a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pictures/sponsor-4.gif" alt="" /></a>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
