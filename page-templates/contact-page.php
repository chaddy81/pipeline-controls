
<?php
/**
 * Template Name: Contact Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package Pipeline Controls
 * @subpackage Pipeline_Controlse
 * @since 2013
 */

get_header(); ?>

	<!-- Main Content -->
	<div id="main" class="contact clearfix">
		<h1>Contact Us</h1>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pictures/map.gif" alt="" />
		<!-- Addresses -->
		<div id="addresses" class="clearfix">
			<div class="column vcard state-a">
				<div class="adr">
					<p><strong><span class="region" title="Atlanta, Georgia">Atlanta, Georgia</span></strong></p>
					<p>
						<span class="street-address">235 Hembree Park Dr<br />Ste 100</span><br />
						<span class="locality">Roswell</span>, <abbr class="region" title="Georgia">GA</abbr> <span class="postal-code">30076</span>
					</p>
					<p class="tel">
						<span class="type">Phone:</span> <span class="value">770.619.5666</span><br />
						<span class="type">Fax:</span> <span class="value">770.619.5510</span>
					</p>
				</div>
			</div>
			<div class="column vcard state-b">
				<div class="adr">
					<p><strong><span class="region" title="Madisonville, Louisiana">Madisonville, Louisiana</span></strong></p>
					<p>
						<span class="street-address">198 Daine Lane</span><br />
						<span class="locality">Madisonville</span>, <abbr class="region" title="Louisiana">LA</abbr> <span class="postal-code">70447</span>
					</p>
					<p class="tel">
						<span class="type">Phone:</span> <span class="value">985.845.7384</span><br />
						<span class="type">Fax:</span> <span class="value">770.619.5510</span>
					</p>
				</div>
			</div>
			<div class="column vcard state-c">
				<div class="adr">
					<p><strong><span class="region" title="Durango, Colorado">Durango, Colorado</span></strong></p>
					<p>
						<span class="street-address">649 Tech Center Drive<br />Suite B</span><br />
						<span class="locality">Durango</span>, <abbr class="region" title="Colorado">CO</abbr> <span class="postal-code">81301</span>
					</p>
					<p class="tel">
						<span class="type">Phone:</span> <span class="value">907.845.7384</span><br />
						<span class="type">Fax:</span> <span class="value">770.619.5510</span>
					</p>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
