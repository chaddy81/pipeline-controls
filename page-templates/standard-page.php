<?php
/**
 * Template Name: Standard Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package Pipeline Controls
 * @subpackage Pipeline_Controlse
 * @since 2013
 */

get_header(); ?>

	<!-- Slider -->
	<div id="slider" class="">
		<div class="slide">
			<?php if(is_page( 'corrosion-control' )) { ?>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pictures/corrosion-control-slide.jpg" alt="" />
			<?php } elseif(is_page( 'controls-and-integration' )) { ?>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pictures/controls-slide.jpg" alt="" />
			<?php } ?>
		<div class="title"><h3><?php the_title(); ?></h3></div>
		</div>
	</div>
	<!-- Main Content -->
	<div id="main" class="clearfix">
		<!-- Thumbs -->
		<?php if(is_page('corrosion-control')){
			get_sidebar( 'corrosion' ); 
		}elseif(is_page('controls-and-integration')){
			get_sidebar( 'controls' );
		} ?>
		<!-- Text -->
		<div id="text">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
			<?php endwhile; ?>
		</div>
	</div>

<?php get_footer(); ?>
